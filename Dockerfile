FROM node:16-alpine

RUN mkdir -p /opt
WORKDIR /opt

COPY .eslint* package.json yarn.lock  /opt/
COPY dist /opt/dist
COPY data  /opt/data
COPY node_modules /opt/node_modules

EXPOSE 80
CMD yarn dist:run
