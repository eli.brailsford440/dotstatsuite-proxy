import { HTTPError } from '../utils/errors'
import { reduce } from 'ramda'

export const initServices = services => evtx => reduce((acc, service) => acc.configure(service), evtx, services)

export const checkApiKey = ctx => {
  const {
    req: {
      headers: { 'x-api-key': xApiKey },
      query: { 'api-key': qApiKey },
    },
  } = ctx.locals
  const {
    config: { apiKey },
  } = ctx.globals()
  if (apiKey !== (xApiKey || qApiKey)) return Promise.reject(new HTTPError(401))
  return Promise.resolve(ctx)
}
