import { getRoutes } from '../router'

const APPS = [
  {
    host: 'H1',
    tenant: 'T1',
    target: 'http://webapp',
  },
  {
    host: 'data-pp',
    path: '/explorer',
    tenant: `oecd`,
    target: 'http://de',
  },
  {
    host: 'pattern',
    path: '/:scope',
    member: 'siscc',
    tenant: ({ member, scope }) => `${member}:${scope}`,
    target: 'http://app1',
  },
  {
    host: 'H2',
    tenant: 'T1',
    target: 'http://h3',
  },
  {
    host: 'H3',
    target: 'http://h3',
  },
  {
    host: 'H4',
    target: 'http://h3',
    forceHttps: true,
    ratePoints: 5,
  },
]

let Routes = getRoutes(() => APPS)

describe('Router', () => {
  it('check1', async () => {
    const req = { url: '/', headers: { host: 'H1' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'T1' },
      target: 'http://webapp/',
      ratePoints: 1,
    }
    expect(app).toEqual(res)
  })

  it('check2', async () => {
    const req = { url: '?tenant=toto&a=b', headers: { host: 'H1' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'T1' },
      target: 'http://webapp?a=b',
      ratePoints: 1,
    }
    expect(app).toEqual(res)
  })

  it.skip('check3', async () => {
    const req = { url: '/sfs/report', headers: { host: 'siscc-staging' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'siscc:sfs' },
      target: 'http://app1/report',
      ratePoints: 1,
    }
    expect(app).toEqual(res)
  })

  it('check4', async () => {
    const req = { url: '/sfs/report', headers: { host: 'H2' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'T1' },
      target: 'http://h3/sfs/report',
      ratePoints: 1,
    }
    expect(app).toEqual(res)
  })

  it('check5', async () => {
    const req = { url: '?tenant=toto', headers: { host: 'H3' } }
    const app = await Routes(req)
    const res = {
      target: 'http://h3?tenant=toto',
      ratePoints: 1,
    }
    expect(app).toEqual(res)
  })

  it('check6', async () => {
    const req = { url: '/sfs/report', headers: { host: 'pattern' } }
    const app = await Routes(req)
    const res = {
      headers: { 'x-tenant': 'siscc:sfs' },
      target: 'http://app1/report',
      ratePoints: 1,
    }
    expect(app).toEqual(res)
  })

  it('check7', async () => {
    const req = { url: '?tenant=toto', headers: { host: 'H4' } }
    const app = await Routes(req)
    const res = {
      target: 'http://h3?tenant=toto',
      forceHttps: true,
      ratePoints: 5,
    }
    expect(app).toEqual(res)
  })
})
