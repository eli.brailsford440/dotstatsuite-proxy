import ROUTES from '../../../data/routes.json'
import debug from '../debug'

export default async ctx => {
  const routes = () => ROUTES
  debug.info(`${ROUTES.length} route(s) loaded`)
  return ctx({ routes })
}
