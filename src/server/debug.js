import pino from 'pino'
import pinoms from 'pino-multi-stream'
import prettifier from 'pino-pretty'
import stackdriver from 'pino-stackdriver'

const options = {
  name: 'proxy',
}

const prettyStream = pinoms.prettyStream({
  prettyPrint: {
    colorize: true,
    translateTime: 'SYS:standard',
    ignore: 'hostname,pid',
  },
  prettifier,
})

let level = process.env.NODE_ENV === 'test' ? 'silent' : process.env.LOGGING_LEVEL || 'info'
// level = 'info';

const streams = [{ level, stream: prettyStream }]

if (process.env.LOGGING_DRIVER === 'gke') {
  const projectId = process.env.LOGGING_PROJECT_ID
  const logName = process.env.LOGGING_LOGNAME
  const stackDriverStream = stackdriver.createWriteStream({
    projectId,
    logName,
    resource: {
      type: 'global',
      labels: {
        app: 'dbmx:proxy',
      },
    },
  })
  streams.push({ level, stream: stackDriverStream })
}

const logger = pino(options, pinoms.multistream(streams))
export default logger
