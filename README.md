# Proxy server

The entry point of an Kubernetes cluster is an `ingress` service, that should allow to route http requests to desired applications.

`ingress` have limited features, so to satisfy our needs we are using a home made `proxy` service behind GCE ingress.


Let's say `app1`, `app2` and `app3` are deployed in the cluster respectively on both `staging` and `qa` environments.

The 2 first apps belong to tenant `t1`, the latter for `t2`.

* url to acces `app1` in staging will be: https://t1.siscc.org/staging/app1
* on `qa`: https://t1.siscc.org/qa/app1
* `app2` on `staging` https://t2.siscc.org/staging/app2

```
  https://<tenant>.siscc.org/<environment>/<application>
```

REM: `<tenant>.siscc.org` could be replaced by a dedicaded DNS entry in tenant's DNS pointing to `kube-rp` @IP

So we have 2 needs:

* route request depending on (`host`, `path`) urls
* set tenants headers depending on `host` to instruct target application of the tenant


## Docker Images

*siscc/dotstatsuite-proxy*

## Probe

`GET /_healthcheck_`

## Gitlab

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy/


## Usage

### Development

Clone repo

```
$ yarn
$ PORT=5007 yarn start:srv
```

## Settings


To add an access to a new application `app1` deployed in `staging` environment on `kube-rp`:

Add entries in `data/routes.json`

```
[
  {
    "host": "kube.tenant1.com",
    "target": "http://app1.staging.svc.cluster.local"
    "tenant": tenant1",
    "path": "/sfs",
    "forceHttps": true,
    "ratePoints": 5
  },
  ...
]
```

1. `kube.tenant1.com` is a tenan1's DNS entry linked to kube-rp@IP
3. `app1` in `target` must match a service name deployed in `staging`
4. `tenant` will be added to destination request to the target service, here `tenant1` may help to get `config` resources.
5. `forceHttps` will redirect if req.headers['x-forwarded-proto'] === 'http' CARE it only works behind a proxy
6. `ratePoints` is the route number of points for a route


### Rate Limiter

`proxy` counts and limits number of routes to manage by ip address and protects from DDoS and brute force attacks at any scale.

It works with MongoDB and in memory as fallback and allows to control requests rate.

Each route costs a number of points (default === 1)

For each request (except '/assets') a rate limiter counter, with an initial value of process.env.RATE_LIMITER_POINTS, is evaluated per IP address, where counter(IP) -= route.points within the time window defined by process.env.RATE_LIMITER_DURATION

When a counter becomes negative, proxy returns status code 429

```
MONGODB_URL=mongodb://mongo:27017
MONGODB_DATABASE=proxy
```
